defmodule PhxappWeb.DataWrapperChannel do
    use Phoenix.Channel

    def join("datawrapper:github", _message, socket) do
        {:ok, socket}
    end

    def handle_in("new_api_request", %{"orgname" => orgname}, socket) do
        payload = ApiWrapper.get_org_repo_names(orgname)
        {:reply, {:ok, payload}, socket}
    end

end
