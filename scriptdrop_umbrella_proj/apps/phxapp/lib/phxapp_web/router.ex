defmodule PhxappWeb.Router do
  use PhxappWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

pipeline :authentication do
    plug BasicAuth, use_config: {:phxapp, :authentication}
end

  scope "/", PhxappWeb do
    pipe_through [:browser, :authentication]

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", PhxappWeb do
  #   pipe_through :api
  # end
end
