# How to Run

1. Be on a recent elixir/erlang, just in case
2. mix deps.get
3. Start server with `mix phx.server`
4. Start webpack with:

```
cd scriptdrop_umbrella_proj/apps/phxapp/assets
npm run watch
```

5. Visit http://localhost:4000
